use std::{
    alloc::{GlobalAlloc, Layout, System},
    sync::atomic::{AtomicUsize, Ordering},
};

static ALLOCATED: AtomicUsize = AtomicUsize::new(0);

struct CounterAllocator;

unsafe impl GlobalAlloc for CounterAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let ret = System.alloc(layout);
        if !ret.is_null() {
            ALLOCATED.fetch_add(layout.size(), Ordering::SeqCst);
        }
        ret
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        System.dealloc(ptr, layout);
        ALLOCATED.fetch_sub(layout.size(), Ordering::SeqCst);
    }
}

#[global_allocator]
static GLOBAL: CounterAllocator = CounterAllocator;

fn main() {
    println!(
        "allocated bytes before main: {}",
        ALLOCATED.load(Ordering::SeqCst)
    );

    let mut v = Vec::new();
    v.push(1);

    println!(
        "allocated bytes after creating Vec: {}",
        ALLOCATED.load(Ordering::SeqCst)
    );

    let mut v2 = Vec::new();
    v2.push(1);

    println!(
        "allocated bytes after creating Vec: {}",
        ALLOCATED.load(Ordering::SeqCst)
    );
}
