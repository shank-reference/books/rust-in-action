fn main() {
    let x: i32 = unsafe { std::mem::zeroed() };
    let sz = std::mem::size_of::<i32>();

    println!("x:{}, bits:{:032b}", x, x);
    println!("sz:{} bytes", sz);
}
