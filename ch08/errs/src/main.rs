use std::net;
use std::{fmt, fs::File, io, net::Ipv6Addr};

#[derive(Debug)]
enum UpstreamError {
    IO(io::Error),
    Parsing(net::AddrParseError),
}

impl fmt::Display for UpstreamError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<io::Error> for UpstreamError {
    fn from(value: io::Error) -> Self {
        UpstreamError::IO(value)
    }
}

impl From<net::AddrParseError> for UpstreamError {
    fn from(value: net::AddrParseError) -> Self {
        UpstreamError::Parsing(value)
    }
}

fn main() -> Result<(), UpstreamError> {
    let _ = File::open("invisible.txt")?;
    let _ = "::1".parse::<Ipv6Addr>()?;

    Ok(())
}
