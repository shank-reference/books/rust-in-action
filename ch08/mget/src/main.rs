use std::net::Ipv4Addr;

use clap::{App, Arg};
use smoltcp::phy::TapInterface;
use url::Url;

mod dns;
mod ethernet;
mod http;

fn main() {
    let app = App::new("mget")
        .about("GET a webpage, manually")
        .arg(Arg::with_name("url").required(true))
        .arg(Arg::with_name("tap-device").required(true))
        .arg(Arg::with_name("dns-server").default_value("1.1.1.1"))
        .get_matches();

    let url_txt = app.value_of("url").unwrap();
    let dns_server_txt = app.value_of("dns-server").unwrap();
    let tap_txt = app.value_of("tap-device").unwrap();

    let url = Url::parse(url_txt).expect("error: unable to parse <url> as a URL");

    if url.scheme() != "http" {
        eprintln!("error: only HTTP protocol supported");
        return;
    }

    let tap = TapInterface::new(&tap_txt)
        .expect("error: unable to use <tap-device> as a network interface");

    let domain_name = url.host_str().expect("domain name required");

    let _ = dns_server_txt
        .parse::<Ipv4Addr>()
        .expect("error: unable to parse <dns-server> as an IPv4 address");

    // let addr = dns::resolve(dns_server_txt, domain_name).unwarp().unwarp();
    // let mac = ethernet::MacAddress::new().into();
    // http::get(tap, mac, addr, url).unwrap();
}
