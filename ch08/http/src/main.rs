use std::{io::prelude::*, net::TcpStream};

fn main() -> std::io::Result<()> {
    let host = "www.google.com:80";

    eprintln!(">> opening connection..");
    let mut conn = TcpStream::connect(host)?;

    eprintln!(">> sending the http request..");
    conn.write_all(b"GET / HTTP/1.0")?;
    conn.write_all(b"\r\n")?;

    conn.write_all(b"Host: www.google.com")?;
    conn.write_all(b"\r\n")?;

    // print to stdout
    eprintln!(">> printing the data received to stdout..");
    std::io::copy(&mut conn, &mut std::io::stdout())?;

    Ok(())
}
