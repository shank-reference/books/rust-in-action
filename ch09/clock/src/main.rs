#![allow(dead_code)]

use chrono::{DateTime, Local, TimeZone, Utc};
use clap::{App, Arg};

#[cfg(not(windows))]
use libc;

const NTP_MESSAGE_LENGTH: usize = 48;
const NTP_TO_UNIX_SECONDS: i64 = 2_208_988_800;
const LOCAL_ADDR: &'static str = "0.0.0.0:12300";

#[derive(Debug, Default, Copy, Clone)]
struct NTPTimestamp {
    seconds: u32,
    fraction: u32,
}

struct NTPMessage {
    // 48 byte buf
    data: [u8; NTP_MESSAGE_LENGTH],
}

#[derive(Debug)]
struct NTPResult {
    t1: DateTime<Utc>,
    t2: DateTime<Utc>,
    t3: DateTime<Utc>,
    t4: DateTime<Utc>,
}

impl NTPResult {
    fn offset(&self) -> i64 {
        let duration = (self.t2 - self.t1) + (self.t4 - self.t3);
        duration.num_milliseconds() / 2
    }

    fn delay(&self) -> i64 {
        let duration = (self.t4 - self.t1) - (self.t3 - self.t2);
        duration.num_milliseconds()
    }
}

impl From<NTPTimestamp> for DateTime<Utc> {
    fn from(ntp: NTPTimestamp) -> Self {
        let secs = ntp.seconds as i64 - NTP_TO_UNIX_SECONDS;
        let mut nanos = ntp.fraction as f64;
        nanos *= 1e9;
        nanos /= 2_f64.powi(32);

        Utc.timestamp(secs, nanos as u32)

        // TODO: continue from here
    }
}

struct Clock;

impl Clock {
    fn get() -> DateTime<Local> {
        Local::now()
    }

    #[cfg(not(windows))]
    fn set<Tz: TimeZone>(t: DateTime<Tz>) {
        use std::mem::zeroed;

        // import platform specific imports withing the function to avoid polluting the global
        // scope
        use libc::time_t;
        use libc::timeval;
        use libc::timezone;
        use libc::{settimeofday, suseconds_t};

        let t = t.with_timezone(&Local);

        let mut tv: timeval = unsafe { zeroed() };
        tv.tv_sec = t.timestamp() as time_t;
        tv.tv_usec = t.timestamp_subsec_micros() as suseconds_t;

        unsafe {
            let null_tz: *const timezone = std::ptr::null();
            settimeofday(&tv as *const timeval, null_tz);
        }
    }
}

fn setup_cli() -> App<'static, 'static> {
    App::new("clock")
        .version("0.1")
        .about("Gets and (aspirationally) sets the time.")
        .arg(
            Arg::with_name("action")
                .takes_value(true)
                .possible_values(&["get", "set"])
                .default_value("get"),
        )
        .arg(
            Arg::with_name("std")
                .takes_value(true)
                .short("s")
                .long("standard")
                .possible_values(&["rfc2822", "rfc3339", "timestamp"])
                .default_value("rfc3339"),
        )
        .arg(
            Arg::with_name("datetime")
                .help("When <action> is 'set', apply <datetime>. Otherwise, ignore."),
        )
}

fn main() {
    let app = setup_cli();
    let args = app.get_matches();

    let action = args.value_of("action").unwrap();
    let std = args.value_of("std").unwrap();

    if action == "set" {
        let t_ = args.value_of("datetime").unwrap();
        let parser = match std {
            "rfc3339" => DateTime::parse_from_rfc3339,
            "rfc2822" => DateTime::parse_from_rfc2822,
            _ => unreachable!(),
        };
        let err_msg = format!("Unable to parse {} according to {}", t_, std);
        let t = parser(t_).expect(&err_msg);

        Clock::set(t);

        let maybe_error = std::io::Error::last_os_error();
        let os_error_code = maybe_error.raw_os_error();
        match os_error_code {
            Some(0) => (),
            Some(_) => eprintln!("Unable to set the time: {:?}", maybe_error),
            None => (),
        }
    }

    let now = Clock::get();
    match std {
        "timestamp" => println!("{}", now.timestamp()),
        "rfc2822" => println!("{}", now.to_rfc2822()),
        "rfc3339" => println!("{}", now.to_rfc3339()),
        _ => unreachable!(),
    }
}
