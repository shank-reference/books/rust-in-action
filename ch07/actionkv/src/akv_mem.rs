use std::{env, path::Path};

use libactionkv::ActionKV;

#[cfg(not(target_os = "windows"))]
const USAGE: &str = "
Usage:
    akv_mem FILE get KEY
    akv_mem FILE delete KEY
    akv_mem FILE insert KEY VALUE
    akv_mem FILE update KEY VALUE
";

fn main() {
    let args: Vec<String> = env::args().collect();
    let fname = args.get(1).expect(&USAGE);
    let action = args.get(2).expect(&USAGE);
    let key = args.get(3).expect(&USAGE);
    let maybe_value = args.get(4);

    let path = Path::new(&fname);
    let mut store = ActionKV::open(path).expect("unable to open file");
    store.load().expect("unable to load data");

    match action.as_str() {
        "get" => match store.get(key.as_ref()).unwrap() {
            None => eprintln!("{:?} not found", key),
            Some(value) => println!("{:?}", value),
        },

        "insert" => {
            let value = maybe_value.expect(&USAGE);
            store.insert(key.as_ref(), value.as_ref()).unwrap();
        }

        "update" => {
            let value = maybe_value.expect(&USAGE);
            store.update(key.as_ref(), value.as_ref()).unwrap();
        }

        "delete" => store.delete(key.as_ref()).unwrap(),

        _ => eprintln!("{}", USAGE),
    }
}
