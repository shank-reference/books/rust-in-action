use std::io::prelude::*;

const BYTES_PER_LINE: usize = 16;

const INPUT: &'static [u8] = br#"
fn main() {
    println!("Hello, world!");
}"#;

fn main() -> std::io::Result<()> {
    let mut buf = vec![];
    INPUT.read_to_end(&mut buf)?;

    let mut pos_in_input = 0;
    for line in buf.chunks(BYTES_PER_LINE) {
        print!("[0x{:08x}] ", pos_in_input);
        for byte in line {
            print!("{:02x} ", byte);
        }
        println!();
        pos_in_input += BYTES_PER_LINE;
    }

    Ok(())
}
